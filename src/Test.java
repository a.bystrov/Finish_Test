import java.util.Scanner;

public class Test {
    public static void main(String[] args) {
        Test theApp = new Test();
        theApp.runTheApp();
    }


    private void runTheApp() {

        Scanner sc = new Scanner(System.in); // создаём объект класса Scanner
        int i;
        int b = 1408;
        System.out.print("Введите четырёхзначный пароль: ");
        if (sc.hasNextInt()) { // возвращает истинну если с потока ввода можно считать целое число
            i = sc.nextInt();// считывает целое число с потока ввода и сохраняем в переменную
            if (i == b) {
                System.out.println("всё верно");
            } else {
                System.out.println("пароль не верен");
                runTheApp();
            }

        }
    }
}
